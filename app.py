from flask import Flask, render_template
import parser_xls
import datetime

app = Flask(__name__)


@app.route('/')
def index() -> 'html':
    menu = parser_xls.make_menu()
    first_half = (len(menu.keys())//2 - 1)
    first_part_menu = dict(list(menu.items())[0:first_half])
    second_part_menu = dict(list(menu.items())[first_half:])

    day = datetime.datetime.today().strftime('%d')
    month = datetime.datetime.today().strftime('%m')
    year = datetime.datetime.today().strftime('%Y')
    a = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября',
         'Декабрь']
    data = day + ' ' + a[int(month) - 1] + ' ' + year

    return render_template('menu.html',
                           first_part_menu=first_part_menu,
                           second_part_menu=second_part_menu,
                           menu=menu,
                           data=data)


if __name__ == '__main__':
    app.run()
