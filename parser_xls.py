import xlrd


# row - строка
# cell - ячейка
rb = xlrd.open_workbook('static/menu.xls')
sheet = rb.sheet_by_index(0)


def make_menu():
    menu = {}
    food = []

    for rownum in range(5, sheet.nrows - 4):
        row = sheet.row_values(rownum)
        if str(row[0]).isupper():
            category = row[0]
            food = []
        else:
            food.append(row)
        menu[category] = food
    return menu


